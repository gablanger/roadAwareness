from collections import namedtuple


Car = namedtuple('Car', 'name direction order sound created_at removed_at')
Lemming = namedtuple('Lemming', 'name direction order created_at removed_at')
ButtonPushed = namedtuple('ButtonPushed', 'time')
UnsafeAware = namedtuple('UnsafeAware', 'time')

AWARE = 'AWARE'
UNSAFE = 'UNSAFE'


class Experiment(object):
    def __init__(self):
        self.users = []


class User(object):
    def __init__(self, name):
        self.name = name
        self.simulations = []

    def __repr__(self):
        return str(self.__dict__)

#TODO: 
class Simulation(object):
    def __init__(self, simType, simID, is_pre_test=False):
        self._simID = simID
        self._simType = simType
        self._is_pre_test = is_pre_test
        self._cars = []
        self._lemmings = []
        self._questions = []
        self._buttons_pushed = []
        self._unsafe_awares = []


    def has_questions(self):
        return bool(questions)

    #TOREMEMBER: list unsafe|awares must be ordered
    #TOREMEMBER: time attributes must be integer, otherwise range function won't work
    def awareness_for_car(self, car, awaretype):
        '''
        Returns the aware time if in car's lifetime
        :param Car car: the car which'll be used to check the attention
        :params str awaretype: aware type, UNSAFE or AWARE
        '''

        if awaretype is UNSAFE:
            awareness = self._unsafe_awares
        elif awaretype is AWARE:
            awareness = self._buttons_pushed

        lifetime = range(car.created, car.removed)
        return next((awr.time for awr in awareness if awr.time in lifetime), None)

    #TOREMEMBER: interval must be a range initial and final time
    def questions_answered(self, interval):
        return set(qst for qst in self.questions if qst.removed_at in interval)

    def questions_info(self, car):
        questions_unsafe, questions_aware = set(), set()

        unsafe = self.awareness_for_car(car, UNSAFE)
        aware = self.awareness_for_car(car, AWARE)

        if unsafe:
            questions_unsafe = self.questions_answered(range(car.created_at, unsafe))
        if aware:
            questions_aware = self.questions_answered(range(car.created_at, aware))

        questions_total = self.questions_answered(range(car.created_at, car.removed_at))

        post_unsafe_questions = questions_total - questions_unsafe
        post_aware_questions = questions_total - questions_aware

        return {
            'total': questions_total,
            'unsafe': questions_unsafe,
            'aware': questions_aware,
            'post_unsafe': post_unsafe_questions,
            'post_aware': post_aware_questions,
        }

    def number_of_questions_aware(self):
        pass

# Question = namedtuple('Question', 'question answer correct_answer created_at removed_at')
class Question(object):
    def __init__(self, question, answer, correct_answer, order, created_at, removed_at):
        self.question = question
        self.answer = answer
        self.correct_answer = correct_answer
        self.created_at = created_at
        self.removed_at = removed_at
        self.order = order
        self.lifetime = float(removed_at) - float(created_at)
        self.respondeu_correto = correct_answer == answer