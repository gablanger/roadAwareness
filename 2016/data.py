#!/usr/bin/env python
#coding: utf8
import os
from os import walk
import re
import sys
import StringIO
import csv
import pandas as pd
import io
from operator import itemgetter
import matplotlib.pyplot as plt
import pylab as pl
import numpy as np
import scipy.stats as stats

from itertools import chain

from roadAwareness import User
from roadAwareness import Simulation
from roadAwareness import Car
from roadAwareness import Lemming
from roadAwareness import Question
from roadAwareness import ButtonPushed
from roadAwareness import UnsafeAware
from roadAwareness import Experiment

left = []
right = []
front = []
back = []

withSound = []
withoutSound = []









def main():
    createExperimentData()

    x = range(4)
    globalMean = []
    globalSTD = []
    


    globalMean.append(np.mean([cel for cel in left]))
    globalMean.append(np.mean([cel for cel in right]))
    globalMean.append(np.mean([cel for cel in front]))
    globalMean.append(np.mean([cel for cel in back ]))


    globalSTD.append(np.std([cel for cel in left]))
    globalSTD.append(np.std([cel for cel in right]))
    globalSTD.append(np.std([cel for cel in front]))
    globalSTD.append(np.std([cel for cel in back]))

    pl.xticks(x, ['left', 'right', 'front', 'back'])
    plt.ylabel('Mean (RED) and STD (BLUE)')
    plt.xlabel('Car direction')
    plt.plot(x, globalMean, 'r')
    plt.plot(x, globalSTD, 'b')




    plt.savefig( os.getcwd() +'/'+ 'globalDirection' + '.png')
    plt.clf()

    x = range(2)
    globalMean = []
    globalSTD = []
    
    globalMean.append(np.mean([row[0] for row in withSound]))
    globalMean.append(np.mean([row[0] for row in withoutSound]))
    globalSTD.append(np.std([row[0] for row in withSound]))
    globalSTD.append(np.std([row[0] for row in withoutSound]))

 

    pl.xticks(x, ['With Sound', 'Without Sound'])
    plt.ylabel('Mean (BLUE) and STD (RED)')
    plt.xlabel('Sound')
    plt.plot(x, globalMean, 'b')
    plt.plot(x, globalSTD, 'r')



    plt.savefig( os.getcwd() +'/'+ 'globalSound' + '.png')
    plt.clf()

    x = range(8)
    globalMean = []
    globalSTD = []






    globalMean.append(np.mean([row[0] for row in withSound if row[1] == 'Left' ]))
    globalMean.append(np.mean([row[0] for row in withoutSound if row[1] == 'Left' ]))     
    globalMean.append(np.mean([row[0] for row in withSound if row[1] == 'Right' ]))
    globalMean.append(np.mean([row[0] for row in withoutSound if row[1] == 'Right' ]))    
    globalMean.append(np.mean([row[0] for row in withSound if row[1] == 'Front' ])) 
    globalMean.append(np.mean([row[0] for row in withoutSound if row[1] == 'Front' ]))    
    globalMean.append(np.mean([row[0] for row in withSound if row[1] == 'Back' ]))
    globalMean.append(np.mean([row[0] for row in withoutSound if row[1] == 'Back' ]))

    globalSTD.append(np.std([row[0] for row in withSound if row[1] == 'Left' ]))
    globalSTD.append(np.std([row[0] for row in withoutSound if row[1] == 'Left' ]))     
    globalSTD.append(np.std([row[0] for row in withSound if row[1] == 'Right' ]))
    globalSTD.append(np.std([row[0] for row in withoutSound if row[1] == 'Right' ]))    
    globalSTD.append(np.std([row[0] for row in withSound if row[1] == 'Front' ])) 
    globalSTD.append(np.std([row[0] for row in withoutSound if row[1] == 'Front' ]))    
    globalSTD.append(np.std([row[0] for row in withSound if row[1] == 'Back' ]))
    globalSTD.append(np.std([row[0] for row in withoutSound if row[1] == 'Back' ]))  



    pl.xticks(x, ['ON L', 'OFF L','ON R', 'OFF R','ON F', 'OFF F','ON B', 'OFF B'])
    plt.ylabel('Mean (BLUE) and STD (RED)')
    plt.xlabel('Sound x direction')
    plt.plot(x, globalMean, 'b')
    plt.plot(x, globalSTD, 'r')



    plt.savefig( os.getcwd() +'/'+ 'globalSoundAndDirection' +"_" + '.png')
    plt.clf()

    h = sorted([row[0] for row in withoutSound] + [row[0] for row in withSound])  #sorted
    fit = stats.norm.pdf(h, np.mean(h), np.std(h))  #this is a fitting indeed

    pl.plot(h,fit,'-o')


    pl.hist(h,normed=True)      #use this to draw histogram of your data
    pl.savefig( os.getcwd() +'/'+ 'someImage' +"_" + '.png')
    plt.clf()

def getLemmingsInf(lemmingsAdded):
    lemmingsLifeTime = []
    lemmingsAdded = sorted(lemmingsAdded, key=itemgetter(3))
    iterator = iter(lemmingsAdded)
    prev_item = None
    current_item = next(iterator)  # throws StopIteration if empty.
    for next_item in iterator:
        prev_item = current_item
        current_item = next_item
        #['User','FileID', 'Initial Time', 'Event Time', 'Time Interval','App','Question','Answer','Right Answer','Cars','Object ID','Object Direction', 'Car Sound', 'Event', 'Position']
        lemmingsLifeTime.append([prev_item[3], current_item[3], prev_item[10], prev_item[11]])
    return lemmingsLifeTime
    

def getCarsInf(carsAdded):
    carsLifeTime = []
    carsAdded = sorted(carsAdded, key=itemgetter(3))
    iterator = iter(carsAdded)
    prev_item = None
    current_item = next(iterator)  # throws StopIteration if empty.
    for next_item in iterator:
        prev_item = current_item
        current_item = next_item
        #['User','FileID', 'Initial Time', 'Event Time', 'Time Interval','App','Question','Answer','Right Answer','Cars','Object ID','Object Direction', 'Car Sound', 'Event', 'Position']
        carsLifeTime.append([prev_item[3], current_item[3], prev_item[10], prev_item[11], prev_item[12]])
    return carsLifeTime


def formatUserData(user, dataFromUser):
    userDataFormated = []

    fileIDs = sorted(list(set([row[1] for row in dataFromUser])))

    #remove 5 cars id.
    #TODO: Should we create a dictionary for simulation data?
    for row in dataFromUser:
        if row[9] == '5 cars' and row[1] in fileIDs:
            fileIDs.remove(row[1])
            break


    mean = []
    std = []
    variance = []
    name = []
    userAwarenesData = []
    x = range(len(fileIDs))


    for fileID in fileIDs:
        simulationData = synchronizeSimulationData(user.name, fileID, [row for row in dataFromUser if row[1] == fileID])
        aSimulation = Simulation(simulationData[0][4], fileID)

        timeLemmingsAdd = [row for row in simulationData if row[13] == 'Lemming added']

        timeLemmingsAdd.append(simulationData[-1])
        lemmingsLifeTime = getLemmingsInf(timeLemmingsAdd)

        lemmingOrder = 0
        for lemming in lemmingsLifeTime:
            aSimulation._lemmings.append(Lemming( lemming[2], lemming[3], lemmingOrder, lemming[0],lemming[1]))
            lemmingOrder += 1


        myQuestions = [row for row in simulationData if row[6] != '-']

        #['User','FileID', 'Initial Time', 'Event Time', 'Time Interval','App','Question','Answer','Right Answer','Cars','Object ID','Object Direction', 'Car Sound', 'Event', 'Position']

        questionsOrder = 0
        for question in myQuestions:
            aSimulation._questions.append(Question( question[6], question[7], question[8], questionsOrder, question[2],question[3]))
            questionsOrder += 1
        #TODO: Only one for, please!
        myUserAware = [row for row in simulationData if row[13] == 'User Aware']
        for myAware in myUserAware:
            aSimulation._buttons_pushed.append(ButtonPushed(myAware[3]))

        myUnsafeAware = [row for row in simulationData if row[13] == 'Unsafe Aware']
        for myUnsafeAware in myUnsafeAware:
            aSimulation._unsafe_awares.append(UnsafeAware(myUnsafeAware[3]))


        timeCarsAdd = [row for row in simulationData if row[13] == 'Car Added']

        timeCarsAdd.append(simulationData[-1])
        carsLifeTime = getCarsInf(timeCarsAdd)





        carOrder = 0;
        timeForAware = []
        for car in carsLifeTime:
            aSimulation._cars.append(Car( car[2], car[3], carOrder, car[4], car[0],car[1]))
            carOrder +=1
            for row in simulationData:
                if row[13] == 'User Aware' and row [3] > car[0] and row[3] < car[1] and row[11] == '-':

                    interval = float(float(row[3] )- float(car[0]))
                    timeForAware.append(interval)

                    if car[3] == 'Left':
                        left.append(interval)
                    if car[3] == 'Right':
                        right.append(interval)
                    if car[3] == 'Back':
                        back.append(interval)
                    if car[3] == 'Front':
                        front.append(interval)
                    if car [4] == 'On':
                        withSound.append([interval,car[3]])
                    if car[4] == 'Off':
                        withoutSound.append([interval,car[3]])

                    break;
                # User 15 do not have an app data.
                elif row[13] == 'User Aware' and row [3] > car[0] and row[3] < car[1] and row[11] != '-':
                    interval = float(float(row[3] )- float(car[0]))
                    timeForAware.append(interval)
                    if row[11] == 'Left':
                        left.append(interval)
                    if row[11] == 'Right':
                        right.append(interval)
                    if row[11] == 'Back':
                        back.append(interval)
                    if row[11] == 'Front':
                        front.append(interval)
                    if row [12] == 'On':
                        withSound.append([interval, row[11]])
                    if row[12] == 'Off':
                        withoutSound.append([interval, row[11]])

                    break;

        if carsLifeTime:

            mean.append(np.mean(timeForAware))
            std.append(np.std(timeForAware))
            variance.append(np.var(timeForAware))
            saveSimulationHistogram(user.name, timeForAware, simulationData[0][5])
            userAwarenesData.extend(timeForAware)
        else:
            mean.append(0)
            std.append(0)            
            variance.append(0)
        name.append(simulationData[0][5])
    
        userDataFormated.extend(sorted(simulationData, key=itemgetter(3)))
        user.simulations.append(aSimulation)   
    pl.xlabel('Simulation')
    pl.ylabel('Mean (RED) and std (BLUE)')
    pl.xticks(x, name)
    plt.plot(x, mean , 'r')
    plt.plot(x, std, 'b')
    plt.savefig( os.getcwd() +'/'+ user.name +"_mean_std"  + '.png')
    plt.clf()  
    h = sorted(userAwarenesData)  #sorted
    fit = stats.norm.pdf(h, np.mean(h), np.std(h))  #this is a fitting indeed (norm.pdf(x) = exp(-x**2/2)/sqrt(2*pi))
    pl.plot(h,fit,'-o')
    pl.hist(h,normed=True)      #use this to draw histogram of your data
    pl.xlabel('Time for aware')
    pl.ylabel('Probability density')
    pl.title('Histogram of IQ: \mu=' + str( np.mean(h)) +', \sigma=' +str(np.std(h)) +'. User: ' + user.name)
    pl.savefig( os.getcwd() +'/'+ user.name + '_histogram' + '.png')
    plt.clf() 
    return userDataFormated 

def saveSimulationHistogram(user, timeForAware, simulationName):
    
    h = sorted(timeForAware)  #sorted
    fit = stats.norm.pdf(h, np.mean(h), np.std(h))  #this is a fitting indeed (norm.pdf(x) = exp(-x**2/2)/sqrt(2*pi))
    pl.plot(h,fit,'-o')
    pl.hist(h,normed=True)      #use this to draw histogram of your data
    pl.xlabel('Time for aware')
    pl.ylabel('Probability density')
    pl.title('Histogram of IQ: \mu=' + str(np.mean(timeForAware)) +', \sigma=' +str(np.std(timeForAware)) +'. \n Simulation:' +simulationName + '. User: ' + user)
    pl.savefig( os.getcwd() +'/'+ user + '_'+ simulationName +"_" + '.png')
    plt.clf()
            

        
def synchronizeSimulationData(user, fileID, dataFromSimulation):

    appData = [row for row in dataFromSimulation if row[5] != '-' and row[3]]
    sceneData = [row for row in dataFromSimulation if row[9] != '-']

    userAwareAppData = [row for row in appData if row[13] == 'User Aware']
    userAwareSceneData = [row for row in sceneData if row[13] == 'User Aware']
    
    sorted(userAwareAppData, key=itemgetter(3))
    sorted(userAwareSceneData, key=itemgetter(3))

    if userAwareAppData and userAwareSceneData:
        offset = float(userAwareSceneData[0][3]) - float(userAwareAppData[0][3])
        for row in sceneData:
            row[3] = float(float(row[3]) - offset)
        for row in appData:
            row[3] = float(row[3])

    sceneData.extend(appData)
    simulationData = sorted(sceneData, key=itemgetter(3))
    simulationType = ''
    for row in simulationData:
        if row[5] == 'Headset on':
            simulationType = 'Headphone'
            break
        elif row[5] == 'Headset off':
            simulationType = 'Trivia'
            break
        elif row[5] == 'Button':
            simulationType= 'Button'
            break
    for row in simulationData:
        row[5] = simulationType

    with open( os.getcwd() +'/'+ simulationData[1][0] +"_" + fileID + '.csv', 'wb') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerows(simulationData)

    return sceneData

def saveExperimentData(experimentData):
    with open( os.getcwd() +'/'+ 'experimentData.csv', 'wb') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerows(experimentData)


def createExperimentData():
    #list of all files location and its name
    paths = getAllPaths(os.getcwd())
    experimentData = []
    uids = set()

    #foreach uid 
    for(dirpath, filename) in paths:
        if('uid' in dirpath):
            uid = re.search('uid\w*', dirpath).group(0)
            uids.add(uid)
            myFile = open(dirpath+'/'+filename)
            data = myFile.read()
            #get the time
            fileID = re.search('\d\d\d\d',filename).group(0)    
            if('CarsData' in filename):
                nCars = re.search('\d+ cars', dirpath).group(0)
                experimentData.extend(formatCarsData(data, uid, nCars,fileID)) 
            elif('LemmingsData' in filename):
                nCars = re.search('\d+ cars', dirpath).group(0)
                experimentData.extend(formatLemmingsData(data, uid,nCars,fileID))
            elif("SoundOff" in filename):
                experimentData.extend(formatTriviaData(data, uid, 'Headset off',fileID))
            elif("SoundOn" in filename):
                experimentData.extend(formatTriviaData(data, uid, 'Headset on',fileID))
            elif ("Button" in filename):
                experimentData.extend(formatButtonData(data, uid, fileID))
    formatedData = []
    formatedData.append(['User','FileID', 'Initial Time', 'Event Time', 'Time Interval','App','Question','Answer','Right Answer','Cars','Object ID','Object Direction', 'Car Sound', 'Event', 'Position'])
    aExperiment = Experiment()
    for user in uids:
        aUser = User(user)
        aExperiment.users.append(aUser)
        formatedData.extend(formatUserData(aUser, [row for row in experimentData if row[0] == user]))
    print aExperiment.users
    saveExperimentData(formatedData)

#using fileIDs to organize simulations.
#toReplace and toReplaceTwo are used to fix timestamps from filename.
def fixFileID(dataFromUser):
    fileIDs = set([row[1] for row in dataFromUser])
    for r in fileIDs:
        toReplace = str(int(r)+1)
        toReplaceTwo = str(int(r)+2)
        if toReplace in fileIDs or toReplaceTwo in fileIDs:
            for row in dataFromUser:
                if row[1] == toReplace or row[1] == toReplaceTwo:
                    row[1] = r
    return dataFromUser

def plotTimeEvent(sceneData, mainPath, user, fileID):

    events = {}
    for row in sceneData:
        if row[13] != '-':
            events.setdefault(row[13], [row[3]]).append(row[3])

    plt.xlabel("X-axis")
    plt.ylabel("Y-axis")
    plt.title("A test graph")
    x = [row[3] for row in sceneData if row[13] == 'Car Added']
    y = []
    cars = [row for row in sceneData if row[13] == 'Car Added']
    cars.append(sceneData[-1])
    y = [(row[3]-0) for row in sceneData if row[13] == 'Car Added']

    plt.plot(x, y)

    plt.savefig( mainPath +'/'+ user +"_" + fileID + '.png')
    plt.clf()

def getAllPaths(path):
    r = []
    for(dirpath, dirnames, filenames) in walk(path):
        if(filenames):
            for filename in filenames:
                r.append([dirpath, filename])
    return r

def formatButtonData(text, uid,fileID):
    sub ='<\?(\w*=?"?\.? ?-?\??)*><(\w* ?:?=?"?(//?)?\.?-?)*>'
    text = re.sub(sub, "", text)
    sub = '<_trivia><time /><answer /><question /><rightAnswer /></_trivia>'
    text = re.sub(sub,"",text)
    sub = '<_puffin><userAware>(\w+\.?\w*\;? ?)+</userAware></_puffin></UserData>'
    text = resolveUserAware(sub, text)
    t = iter(text)
    prevTime = 0
    table = []
    for te in text:
        currentTime = t.next()
        table.append([uid, fileID, prevTime, currentTime, '-', 'Button', '-', '-', '-','-','-','-','-','User Aware','-'])
        prevTime = currentTime
    return table

def formatTriviaData(text, uid, sound, fileID):
    sub ='<\?(\w*=?"?\.? ?-?\??)*><(\w* ?:?=?"?(//?)?\.?-?)*>'
    text = re.sub(sub, "", text)
    sub = '<_trivia><time>((\w*\.?\w*); ?)*</time>'
    time = resolveTime(sub, text)
    text = re.sub(sub, "", text)
    sub = '<answer>((-?\w*\.?\,? ?)*\w*; )*</answer>'
    answer = resolveAnswer(sub, text)
    text = re.sub(sub, "", text)
    text = str(text)
    text = text.replace('×', 'x')
    text = text.replace('–', '-')
    sub = '<question>((\(?\-?\+?\w+(:? ? ?\+?\-?\,? ? ?\(?\w+\)?)*\)?)*\??\;? ? ?)+</question>'
    question = re.search(sub, text).group(0)
    question = resolveQuestion(sub, text)
    text = re.sub(sub, "", text)
    sub = '<rightAnswer>(\-?\w+(\.?\,? ?\w*)*; )+</rightAnswer></_trivia>'
    rightAnswer = resolveRightAnswer(sub, text)
    text = re.sub(sub, "", text)
    sub = '<_puffin><userAware>(\w+\.?\w*\;? ?)+</userAware></_puffin></UserData>'
    userAware = resolveUserAware(sub, text)
    text = re.sub(sub, "", text)
    t = iter(time)
    a = iter(answer)
    ra = iter(rightAnswer)
    q = iter(question) 
    ua = iter(userAware)
    table = []
    prevTime = 0
    for quest in question:
        currentTime = t.next()
        if currentTime:
            diffTime = float(float(currentTime) - float(prevTime))
            table.append([uid, fileID, prevTime, currentTime, diffTime , sound, q.next(), a.next(), ra.next(), '-','-','-','-','-','-'])
            prevTime = currentTime

    for uar in userAware:
        uAware = ua.next()
        table.append([uid, fileID, '-', uAware, '-', sound, '-', '-','-','-','-','-','-','User Aware','-'])
    return table

def resolveUserAware(sub, text):
    userAware = re.search(sub, text).group(0)
    sub = '<_puffin><userAware>'
    userAware = re.sub(sub, "", userAware)
    sub = '\ufeff'
    userAware = re.sub(sub,"", userAware)
    sub = '</userAware></_puffin></UserData>'
    userAware = re.sub(sub, "", userAware)
    sub = '; '
    userAware = re.sub(sub, "\n", userAware)
    userAware = userAware.split('\n')
    return userAware


def resolveTime(sub, text):
    time = re.search(sub, text).group(0)
    sub = '<_trivia><time>'
    time = re.sub(sub, "", time)
    sub = '</time>'
    time = re.sub(sub, "", time)
    sub = '; '
    time = re.sub(sub, "\n", time)
    time = time.split('\n')
    return time

def resolveAnswer(sub, text):
    answer = re.search(sub, text).group(0)
    sub = '<answer>'
    answer = re.sub(sub, "", answer)
    sub = '</answer>'
    answer = re.sub(sub, "", answer)
    sub = '; '
    answer = re.sub(sub, "\n", answer)
    answer = answer.split('\n')
    return answer

def resolveRightAnswer(sub, text):
    answer = re.search(sub, text).group(0)
    sub = '<rightAnswer>'
    answer = re.sub(sub, "", answer)
    sub = '</rightAnswer></_trivia>'
    answer = re.sub(sub, "", answer)
    sub = '; '
    answer = re.sub(sub, "\n", answer)
    answer = answer.split('\n')
    return answer

def resolveQuestion(sub, text):
    question = re.search(sub, text).group(0)
    sub = '<question>'
    question = re.sub(sub, "", question)
    sub = '</question>'
    question = re.sub(sub, "", question)
    sub = '; '
    question = re.sub(sub, "\n", question)
    question = question.split('\n')
    return question


def formatLemmingsData(text, uid, nCars, fileID):
    sub ='<\?(\w*=?"?\.? ?-?\??)*><(\w* ?:?=?"?(//?)?\.?-?)*>'
    text = re.sub(sub, "", text)
    sub = '<LemmingData><_name>'
    text = re.sub(sub, "", text)
    sub = '</_name><_time>'
    text = re.sub(sub, "; ", text)
    sub = '</_time><_position>'
    text = re.sub(sub, " ", text)
    sub = '</_position></LemmingData>'
    text = re.sub(sub, ";\n", text)
    sub = '</ArrayOfLemmingData>'
    text = re.sub(sub, "", text)
    text = text.replace(' Lemming added at ', 'Lemming added;')
    text = text.replace('Pavement stop at ', 'Pavement stop;')
    text = text.replace('Lemming Ran Over at ', 'Lemming Ran Over;')
    text = text.replace('Safe at ', 'Safe;')
    text = text.split('\n')
    table = [];
    for row in text:
        rowSplit = row.split(';')
        lemming = rowSplit.pop(0)
        lemmingDirection = re.sub('CatLemmingF\w*','Front',lemming)
        if lemmingDirection != 'Front':
            lemmingDirection = re.sub('CatLemmingB\w*','Back',lemming)
        if lemmingDirection == '﻿CatLemmingF0':
            lemmingDirection = 'Front'
        event = []
        position = []
        time = []
        for cell in rowSplit:
            if 'stop' in cell or 'added' in cell or 'Ran' in cell or 'Safe' in cell:
                event.append(cell)
            elif ',' in cell:
                position.append(cell)
            elif re.compile('[0-9]').match(cell):
                time.append(cell)
        p = iter(position)
        t = iter(time)
        for e in event:
            localTime = t.next()
            table.append([uid, fileID, '-', localTime, '-','-','-','-','-', nCars, lemming, lemmingDirection,'-',e, p.next()])
    return table



def formatCarsData(text, uid, nCars, fileID):
    sub ='<\?(\w*=?"?\.? ?-?\??)*><(\w* ?:?=?"?(//?)?\.?-?)*>'
    text = re.sub(sub, "", text)
    sub = '<CarDataObject><_name>'
    text = re.sub(sub, "", text)
    sub = '</_name><_time>'
    text = re.sub(sub, "; ", text)
    sub = '</_time><_position>'
    text = re.sub(sub, " ", text)
    sub = '</_position><_unsafe>'
    text = re.sub(sub, " ", text)
    sub = '</_unsafe></CarDataObject>'
    text = re.sub(sub, ";\n", text)
    sub = '</ArrayOfCarDataObject>'
    text = re.sub(sub, "", text)
    text = text.replace('\xef\xbb\xbf', '')
    text = text.replace('; ', ';')
    text = text.replace('Car added at ', 'Car Added;')
    text = text.replace('User aware at ', 'User Aware;')
    text = text.replace('Unsafe aware at ', 'Unsafe Aware;')
    text = text.replace(u'\ufeff', '')
    text = text.split('\n')
    table = [];
    for row in text:
        rowSplit = row.split(';')
        car = rowSplit.pop(0)
        #TODO: refactoring
        car = re.sub('Car01F\d','car01;Front;On',car)
        car = re.sub('Car03F\d','car03;Front;Off',car)
        car = re.sub('Car01B\d','car01;Back;On',car)
        car = re.sub('Car03B\d','car03;Back;Off',car)
        car = re.sub('Car01L\d','car01;Left;Off',car)
        car = re.sub('Car02L\d','car02;Left;On',car)
        car = re.sub('Car01R\d','car01;Right;On',car)
        car = re.sub('Car03R\d','car03;Right;Off',car)
        carSplit = car.split(';')
        event = []
        position = []
        time = []
        for cell in rowSplit:
            if 'Aware' in cell or 'Added' in cell:
                event.append(cell)
            elif ',' in cell:
                position.append(cell)
            elif re.compile('[0-9]').match(cell) :
                time.append(cell)
        p = iter(position)
        t = iter(time)
        for e in event:
            myTime = t.next()
            myPosition = p.next()
            table.append([uid, fileID, '-', myTime, '-', '-','-','-','-',nCars, carSplit[0], carSplit[1],carSplit[2],e, myPosition])

    return table
    

main()

