import json
from pprint import pprint
import os
import re
import csv
import numpy as np


#GOAL: Car - Added - Removed - Sound - Ocluded - timeForAware- safeAware - Safe - UID

carsPaths = {"path1":{
    "wayPoints":{
        "wp1":{ "x": 142.7,"y": 0,"z": -64.3},
        "wp2":{ "x": 142.7,"y": 0,"z": -15.4},
        "wp3":{ "x": -61,"y": 0,"z": -15.4},
        "wp4":{ "x": -149.5,"y": 0,"z": -15.4},
        "wp5":{ "x": -149.5,"y": 0,"z": -64.8}
        },
    "direction": "Front"
},
"path2":{
    "wayPoints":{
        "wp1":{ "x": -267.1,"y": 0,"z": 36.2},
        "wp2":{ "x": -267.1,"y": 0,"z": -13.0},
        "wp3":{ "x": -61,"y": 0,"z": -13.0},
        "wp4":{ "x": 26.5,"y": 0,"z": -13.0},
        "wp5":{ "x": 26.5,"y": 0,"z": 82}
        },
    "direction": "Back"
},
"path3":{
    "wayPoints":{
        "wp1":{ "x": -30.6,"y": 0,"z": 212.3},
        "wp2":{ "x": -30.6,"y": 0,"z": -15.4},
        "wp3":{ "x": -61,"y": 0,"z": -15.4},
        "wp4":{ "x": -149.5,"y": 0,"z": -15.4},
        "wp5":{ "x": -149.5,"y": 0,"z": -64.8}
        },
    "direction": "FrontLeft"
},
"path4":{
    "wayPoints":{
        "wp1":{ "x": -32.84,"y": 0,"z": -241},
        "wp2":{ "x": -32.84,"y": 0,"z": -15.4},
        "wp3":{ "x": -61,"y": 0,"z": -15.4},
        "wp4":{ "x": -148.2,"y": 0,"z": -15.4},
        "wp5":{ "x": -148.2,"y": 0,"z": -92}
        },
    "direction": "FrontRight"
},
"path5":{
    "wayPoints":{
        "wp1":{ "x": -90.17,"y": 0,"z": 212.3},
        "wp2":{ "x": -90.17,"y": 0,"z": -13.0},
        "wp3":{ "x": -61,"y": 0,"z": -13.0},
        "wp4":{ "x": 26.5,"y": 0,"z": -13.0},
        "wp5":{ "x": 26.5,"y": 0,"z": 82}
        },
    "direction": "BackLeft"
},
"path6":{
    "wayPoints":{
        "wp1":{ "x": -90.4,"y": 0,"z": -241},
        "wp2":{ "x": -90.4,"y": 0,"z": -13.0},
        "wp3":{ "x": -61,"y": 0,"z": -13.0},
        "wp4":{ "x": 26.5,"y": 0,"z": -13.0},
        "wp5":{ "x": 26.5,"y": 0,"z": 58}
        },
    "direction": "BackRight"
}}

defaultSafeSide = {"Safe":"Front","Time":0,"x":0.1992155,"y":0.4543561,"z":1.405861,"Rx":-0.2583797,"Ry":0.2267305,"Rz":-0.6287609}

def getAllPaths(path):
    r = []
    for(dirpath, dirnames, filenames) in os.walk(path):
        if(filenames):
            for filename in filenames:
                if filename.endswith('.json'):
                    r.append([dirpath, filename])
    return r

def createUserPaths(paths):
    uids = set()
    userPaths = {}
    for(dirpath, filename) in paths:
        if('uid' in dirpath):
            uid = re.search('uid\w*', dirpath).group(0)
            uids.add(uid)

    for ID in uids:
        userPaths[ID] = []
        for (dirpath, filename) in paths:
            if '/'+ID in dirpath:
                userPaths[ID].append((dirpath,filename))


    return userPaths

def fixSimulationPaths(simulationPaths):

    iterator = sorted(list(simulationPaths.keys()))

    deadIDs = []

    for ID in iterator:
        if ID not in deadIDs:
            if str(int(ID)+1) in iterator:
                simulationPaths[ID].extend(simulationPaths[str(int(ID)+1)])
                del simulationPaths[str(int(ID)+1)]
                deadIDs.append(str(int(ID)+1))

            if (str(int(ID)+2)) in iterator:
                simulationPaths[ID].extend(simulationPaths[str(int(ID)+2)])
                del simulationPaths[str(int(ID)+2)]
                deadIDs.append(str(int(ID)+2))

    return simulationPaths



def createSimulationPaths(userPaths):

    simulationIDs = set()

    simulationPaths = {}

    for (dirpath, filename) in userPaths:
        simulationIDs.add(re.search('\d\d\d\d',filename).group(0))
        uid = re.search('uid\w*', dirpath).group(0)

    for ID in simulationIDs:
        simulationPaths[ID] = []
        for (dirpath, filename) in userPaths:

            
            if ID in filename:
                simulationPaths[ID].append((dirpath,filename))



    simulationPaths = fixSimulationPaths(simulationPaths)

    return simulationPaths


def createObjectPaths(simulationPaths):

    objectPaths = {}

    types = [ 'App', 'Scene']

    for tag in types:
        for (dirpath, filename) in simulationPaths:
            if tag in filename:
                objectPaths[tag] = dirpath+'/'+filename

    return objectPaths


def createExperimentPaths():

    paths = getAllPaths(os.getcwd() )
    
    paths = [list(tuples) for tuples in paths]

    # userPaths = {'uid':[(dirpath,filename)...]...}
    userPaths = createUserPaths(paths)
    # simulationPaths = {'uid': {'simulation': [(dirpath,filename)...]...}... }
    simulationPaths = {}

    for user in userPaths:
        simulationPaths[user] = createSimulationPaths(userPaths[user])

    objectPaths = {}

    for user in simulationPaths:
        objectPaths[user] = {}
        for simulation in simulationPaths[user]:
            objectPaths[user][simulation] = createObjectPaths(simulationPaths[user][simulation])


    return objectPaths

def fixSceneTime(scene, firstAware):
    rightTime = scene['AwareCars'][0]['Time'] - firstAware 
    for htd in scene['HeadTracking']:
       if htd: 
           htd['Time'] = htd['Time'] - rightTime
    for ss in scene['SafeSide']:
       if ss: 
           ss['Time'] = ss['Time'] - rightTime
    for ac in scene['AwareCars']:
       if ac:
           ac['Time'] = ac['Time'] - rightTime
    for car in scene['Cars'].values():
        car['timeAdded'] = car['timeAdded'] - rightTime
        car['timeRemoved'] = car['timeRemoved'] - rightTime
        car['timeCritical'] = car['timeCritical'] - rightTime

    return scene
  
def fixAppTime(app, firstAware):
    # Filter button app case
    if 'Death' in app.keys():
        deaths = app['Death']
        for deathID, death in deaths.items():
            death['time'] = (firstAware['Time'] - app['CarAware'][0]) + death['time']
        # TODO: fix app RoundStartTime and CarAware time
    return app

def getCarsOrdered():
    return ["Car0", "Car1", "Car2", "Car3", "Car4", "Car5", "Car6", "Car7", "Car8", "Car9",
            "Car10", "Car11"]

def createSimulationDataObject(simulationPaths):

    #TODO: refactoring
    dirpath = simulationPaths.items()[0][1]['App']
    user = re.search('uid\w*', dirpath).group(0)
   

    simulations = []
    myCars = []
    app = ""
    for simulation in simulationPaths:
        scene = json.load(open(simulationPaths[simulation]['Scene']))
        
        if 'App' in simulationPaths[simulation]:
            app = json.load(open(simulationPaths[simulation]['App']))
            app = fixAppTime(app, scene['AwareCars'][0])
        
        #scene = fixSceneTime(scene, app['CarAware'][0])
        #TODO: getting informations 
        simID = simulation
        if app:
            simType = app['SimType']
        else:
            simType = "SoundOff"
        if simType != "Button":
            if app:
                appDeaths = len(app['Death'])
                points = max(app['Death'].values(),key=lambda item:item['points'])['points'];
        #GOAL: Car - Added - Removed - Sound - Ocluded - timeForAware - direction - timeCritical - safeAware - Safe - runOver - SimType - UID
        

        # Testing Syncronization between app and scene times
        #print app['CarAware']
        #print scene['AwareCars']
        #input()
        lastCriticalTime = 0
        lastPoints = 0
        for name in getCarsOrdered():
            car = scene['Cars'][name]
            aCar = []
            aCar.append(user)
            aCar.append(name)
            #aCar.append(car['name'])
            aCar.append(car['timeAdded'])
            aCar.append(car['timeRemoved'])
            aCar.append(car['sound'])
            aCar.append(car['route'] in ('path3', 'path4', 'path5')) # is ocluded?
            
            timeForAware = '-'
            for awareCar in scene['AwareCars']:
                if awareCar:
                    if awareCar['name'] == car['name']:
                        timeForAware = awareCar['Time']
                        break
            
            aCar.append(timeForAware)
            direction = carsPaths[car['route']]['direction']
            aCar.append(direction)
            timeCritical = car['timeCritical']
            aCar.append(timeCritical)
            # TODO: safeAware
            laneChangesSmaller = []
            laneChangesBigger = []
            safeWithDefault = []
            safeWithDefault.append(defaultSafeSide)
            safeWithDefault.extend(scene['SafeSide'])
            #a = np.argmax(x['Time'] for x in safeWithDefault if x['Time'] < car['timeCritical'])


            for side in safeWithDefault:
                # TODO: solve empty sides
                if side:
                    if side['Time'] < car['timeCritical']:
                        laneChangesSmaller.append(side)
                    else:
                        laneChangesBigger.append(side)
            laneChangesSmaller = sorted(laneChangesSmaller, key=lambda k: k['Time']) 
            laneChangesBigger = sorted(laneChangesBigger, key=lambda k: k['Time']) 
           


            if laneChangesSmaller:
                aCar.append(laneChangesSmaller[-1]['Time'])
                aCar.append(laneChangesSmaller[-1]['Safe'])
            if laneChangesBigger:
                aCar.append(laneChangesBigger[0]['Time'])
                aCar.append(laneChangesBigger[0]['Safe'])
            else:
                aCar.append('-')
                aCar.append('-')


            runOver = False
            # TODO: Review empty sides
            if laneChangesSmaller:
                if direction in ('Front', 'FrontLeft', 'FrontRight') and laneChangesSmaller[-1]['Safe'] == 'Back':
                    runOver = True
                elif direction in ('Back', 'BackLeft', 'BackRight') and laneChangesSmaller[-1]['Safe'] == 'Front':
                    runOver = True

            aCar.append(runOver)
            aCar.append(simType)

            points = "-"
            numDeaths = "-"
            maxObstacle = "-"
            # App Data
            if 'Death' in app.keys():
                points = 0
                numDeaths = 0
                maxObstacle = 1
                deaths = app['Death']
                for deathID, death in deaths.items():
                    total_points = death['points']
                    deathTime = death['time']
                    
                    if (deathTime > lastCriticalTime) and (deathTime <= timeCritical):
                        numDeaths += 1
                        #print user + " - " + name + " - " + deathID
                        if (total_points - lastPoints) > points:
                            points = total_points - lastPoints

                        obstacleNumber = death['obstacleNumber']
                        if obstacleNumber > maxObstacle:
                            maxObstacle = obstacleNumber
            aCar.append(points)
            aCar.append(numDeaths)
            aCar.append(maxObstacle)
            aCar.append(timeCritical - lastCriticalTime)    

            lastCriticalTime = timeCritical
            lastPoints = points

            awareCar = []

            myCars.append(aCar)

    
    return myCars







def createHeadTrackingPlot(simulationPaths):

    for simulation in simulationPaths:
        scene = json.load(open(simulationPaths[simulation]['Scene']))
        




objects = createExperimentPaths()




toCSV = []
toCSV.append(["User", "Car", "Added", "Removed", "Sound", "Is Ocluded", "Time for Aware",
    "Direction", "Time Critical", "Moved to Current Lane", "Safe Lane", "Moved to next Lane", 
    "Next Lane", "Run Over", "Simulation Type", "Points", "Deaths", "Max Obstacle", "Time between Critical times"])

for user in objects:
    toCSV.extend(createSimulationDataObject(objects[user]))



with open( os.getcwd() +'/'+ 'experimentDataParser.csv', 'w') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerows(toCSV)


#data = json.load(open('Scene124317.json'))


#for car in data['Cars'].values():
#   myCar = []
#   for aware in data['AwareCars']:
#       if aware and car:
#           if car['timeAdded'] < aware['Time'] and car['timeRemoved'] > aware['Time']:
#               for safe in data['SafeSide']:
#                   if safe:
#                       if car['timeRemoved'] > safe['Time']:
#                           print car['name']
#                           print safe['Time']


"""
APP:
    SimID
    SimType
    RoundStartTime
    CarAware
    Death:
        points
        time
        speed
        ball_color
        hit_color (?)
        obstacle_type
        obstacle_num

Scene:
    SimID
    SimType
    EnvironmentType
    HeadTracking:
        time
        position
        rotation
    SafeSide:
        safe(front|back)
        time
        position
    AwareCars:
        time
        position
        name
    Cars:
        timeCritical
        sound
        name
        timeAdded
        timeRemoved
        positionRemoved
        route

"""






